/**
 *
 **/ 
module.exports = {

//pause all other creep spawns EXCEPT spawn slave

//if 2 enemies, spawn a small creep dedicated to filling towers

//if more are detected, spawn defensive creeps

//if a structure dies, activate safe mode

    run: function(room) {
        if (Game.time%10 == 0) {
            if(room.controller && room.controller.my){
                let hostileCreeps = room.find(FIND_HOSTILE_CREEPS);
                if (hostileCreeps.length > 3 && room.controller.level < 4 && !room.controller.safeMode) {
                    room.controller.activateSafeMode();
                    Game.notify('Room: ' + room.name + ' safe mode activated');
                    console.log('Room: ' + room.name + ' safe mode activated');
                }
            }
        }
    }

};
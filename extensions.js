Creep.prototype.sayName = function(name = 'test') {
    console.log(name);
}

Creep.prototype.renew = function() {
    let homeRoom = Game.rooms[this.memory.home];
    if (homeRoom.spawns.length > 0 ) {
        let renewResult = homeRoom.spawns[0].renewCreep(this);
        //if this is not my spawn, we need to go home cause we are drunk
        if (renewResult == ERR_NOT_IN_RANGE) {
            this.moveTo(homeRoom.spawns[0]);
        } else if (renewResult == ERR_NOT_OWNER){
            this.moveTo(homeRoom.controller);
        }
    }
}

Creep.prototype.assignSource = function(roomName = this.room.name) {
    
    let room = Game.rooms[roomName];
    let creepSources = {};
    
    if (!room){
        console.log('no room');
        return;
    }
    
    
    //Initalize creepSources key values.  This fixes issues with empty rooms.
    room.sources.forEach(source => {
        creepSources[source.id] = 0;
    });
    
    let creeps =  _.filter(Game.creeps, {memory : {role: this.memory.role, destination: roomName}});
    
    creeps.forEach( myCreep => {
        //Only do this if the creep has a source declared
        if (myCreep.memory.source && creepSources.hasOwnProperty(myCreep.memory.source)) {
            //add a tally for each creep at each source
            creepSources[myCreep.memory.source] = creepSources[myCreep.memory.source] + 1;
        }
    });
        
    //give this creep the source with the least number of creeps assigned to it
    let creepSourceCount = 99;
    let targetSource = null;
    for (var property in creepSources) {
        if (creepSources.hasOwnProperty(property)) {
            if (creepSources[property] < creepSourceCount) {
                creepSourceCount = creepSources[property];
                targetSource = property;
            }
        }
    }
    this.memory.source = targetSource;
    //console.log(JSON.stringify(creepSources));
    //console.log("Assigning creep " + this.name + " to sourceID " + targetSource);

}

Creep.prototype.setPathTo = function(roomObject) {
    let path = this.pos.findPathTo(roomObject);
    this.memory.path = Room.serializePath(path);
}

Creep.prototype.smartAttack = function(target) {
    if (this.attack(target) == ERR_NOT_IN_RANGE) {
        this.moveTo(target);
    }
}

Creep.prototype.smartBuild = function() {
    if(this.build(this.room.constructionSites[0]) == ERR_NOT_IN_RANGE) {
        this.moveTo(this.room.constructionSites[0]);
    }
}

Creep.prototype.buildClose = function() {
    let buildTargets = this.pos.findInRange(this.room.constructionSites, 3);
    if(buildTargets.length > 0){
        this.build(buildTargets[0]);
    }
}

Creep.prototype.smartRepair = function() {
    if(this.repair(this.room.damagedStructures[0]) == ERR_NOT_IN_RANGE) {
        this.moveTo(this.room.damagedStructures[0]);
    }
}

Creep.prototype.repairClose = function() {
    let repairTargets = this.pos.findInRange(this.room.damagedStructures, 3);
    if(repairTargets.length > 0){
        this.repair(repairTargets[0]);
        return 1;
    }
    else{
        return 0;
    }
}

Creep.prototype.smartHarvest = function(source = this.source) {
    let sourceObject = Game.getObjectById(source);
    if (this.harvest(sourceObject) == ERR_NOT_IN_RANGE) {
        this.moveTo(sourceObject);
    }
}

Creep.prototype.smartDeliver = function() {
    let destination = Game.rooms[this.memory.home].storage;
    if (this.transfer(destination, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
        this.moveTo(destination);
    }
}

Creep.prototype.getSourceContainer = function() {
    let sourcePos = Game.getObjectById(this.memory.source).pos;
    let containers = sourcePos.findInRange(FIND_STRUCTURES, 2, {
        filter: { structureType: STRUCTURE_CONTAINER }
    });
    
    containers = containers.sort( (a,b) => {
        return a.store[RESOURCE_ENERGY] - b.store[RESOURCE_ENERGY];
    });
    
    return containers[containers.length -1];
}

Room.prototype.getCreepCount = function() {
    
    let creepTypeCount = {
        workHorses  : 0,
        spawnSlaves : 0,
        harvesters  : 0,
        upgraders   : 0,
        transporters: 0,
        builders    : 0,
        warriors    : 0,
        rangers     : 0,
    };
    
    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        if (creep.room.name != this.name){
            continue;
        }
        if (creep.memory.role == 'workHorse') {
            creepTypeCount.workHorses++;
        }
        if (creep.memory.role == 'spawnSlave') {
            creepTypeCount.spawnSlaves++;
        }
        if (creep.memory.role == 'harvester') {
            creepTypeCount.harvesters++;
        }
        if (creep.memory.role == 'upgrader') {
            creepTypeCount.upgraders++;
        }
        if (creep.memory.role == 'transporter') {
            creepTypeCount.transporters++;
        }
        if (creep.memory.role == 'builder') {
            creepTypeCount.builders++;
        }
        if (creep.memory.role == 'warrior') {
            creepTypeCount.warriors++;
        }   
        if (creep.memory.role == 'ranger') {
            creepTypeCount.rangers++;
        }
    }
    
    return creepTypeCount;
}

Room.prototype.setRoomObjects = function() {

    this.constructionSites = this.find(FIND_CONSTRUCTION_SITES, {filter: site => (site.my)});
    
    this.spawnStructures = this.find(FIND_STRUCTURES, {
        filter: structure => ( 
            structure.structureType == STRUCTURE_SPAWN ||
            structure.structureType == STRUCTURE_EXTENSION ||
            (structure.structureType == STRUCTURE_TOWER && structure.energyCapacity - structure.energy > 99)) && structure.energy < structure.energyCapacity
        });
    
    
    let durability = 90000;
    this.damagedStructures = this.find(FIND_STRUCTURES, {
        filter: (structure) => {
            return (
                (structure.structureType == STRUCTURE_ROAD && structure.hits < structure.hitsMax) || 
                (structure.structureType == STRUCTURE_CONTAINER && structure.hits < structure.hitsMax) ||
                (structure.structureType == STRUCTURE_SPAWN && structure.hits < structure.hitsMax) ||
                (structure.structureType == STRUCTURE_RAMPART && structure.hits < durability) ||
                (structure.structureType == STRUCTURE_WALL && structure.hits < durability)
            );
        }
    });
    
    this.spawns = this.find(FIND_MY_SPAWNS);
    
    this.sources = this.find(FIND_SOURCES);
    
    this.myCreeps = this.find(FIND_MY_CREEPS);

    this.hostileCreeps = this.find(FIND_HOSTILE_CREEPS);

    //this.droppedEnergy = this.find(FIND_DROPPED_ENERGY, {filter: resource => resource.amount > 100});
}

Room.prototype.myRoom = function() {
    if (this.controller.my || (this.controller.reservation && this.controller.reservation.username == 'Hive_')) {
        return true;
    } else {
        return false;
    }
}

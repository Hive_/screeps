var roleBuilder = {

    /** @param {Creep} creep **/
    run: function(creep) {

        if(creep.memory.building && creep.carry.energy == 0) {
            creep.memory.building = false;
        }
        if(!creep.memory.building && creep.carry.energy == creep.carryCapacity) {
            creep.memory.building = true;
        }

        if(creep.memory.building) {
            if(creep.room.constructionSites.length) {
                if(creep.build(creep.room.constructionSites[0]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(creep.room.constructionSites[0]);
                }
            }else{
                /*
                var targets = creep.room.find(FIND_STRUCTURES, {
                    filter: object => {
                        if (object.structureType == STRUCTURE_WALL || object.structureType == STRUCTURE_RAMPART) {
                            return object.hits < 10000;
                        } else if (object.structureType != STRUCTURE_ROAD || object.structureType != STRUCTURE_CONTAINER) {
                            return object.hits < object.hitsMax;
                        }
                    }
                });

                targets.sort((a,b) => a.hits - b.hits);
                
                if(targets.length > 0) {
                    if(creep.repair(targets[0]) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(targets[0]);    
                    }
                }*/
            }
        }
        else {
            
            if(creep.withdraw(creep.room.storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.storage);
            }
            
        }
        
    }
};

module.exports = roleBuilder;
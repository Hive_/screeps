
var roleScout = {
    run: function(creep) {
        
        if (creep.pos.getRangeTo(Game.flags['scout'].pos) == 0) {
            creep.memory.scout_reached = 1;
        }
        
        if (creep.memory.scout_reached != 1) {
            creep.moveTo(Game.flags['scout']);
            return;
        }
        
        /*
        var constructionSites = creep.room.find(FIND_CONSTRUCTION_SITES);
        if (constructionSites.length > 0) {
            creep.moveTo(constructionSites[0]);
        } 
        */
    }
};


module.exports = roleScout;

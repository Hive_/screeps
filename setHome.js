/**
 * One time use function to set the home of all creeps to their current room.
 **/

var setHome = {
    
    run: function() {
        var creeps = Game.creeps;
        for(var i in Game.creeps) {
            if(Game.creeps[i].memory.role == 'workHorse'){
                console.log(Game.creeps[i]);
                Game.creeps[i].memory.home = 'E55S77';
                Game.creeps[i].memory.home_reached = 0;
            }
        }
    }
}

module.exports = setHome;
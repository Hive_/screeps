
var roleScout = {
    run: function(creep) {
        
        if (creep.pos.getRangeTo(Game.flags[creep.memory.destination].pos) == 0) {
            creep.memory.squat_reached = 1;
        }
        
        if (creep.memory.squat_reached != 1) {
            creep.moveTo(Game.flags[creep.memory.destination]);
            return;
        }

    }
};


module.exports = roleScout;

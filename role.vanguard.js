
var roleVanguard = {
    run: function(creep) {
        
        if (!creep.memory.attack) {
            creep.memory.attack = false;
        }
        
        if (creep.pos.getRangeTo(Game.flags['warpath'].pos) == 0) {
            creep.memory.warpath_reached = 1;
        }
        //if (creep.room.name == Game.flags['wartarget'].room.name) {
        //    creep.memory.wartarget_reached = 1;
        //}
        
        if (creep.memory.warpath_reached != 1) {
            creep.moveTo(Game.flags['warpath']);
            return;
        }
        if (creep.memory.attack == true) {
            //Just move to the spot.
            creep.moveTo(Game.flags['wartarget']);
        }
        
        
        //If creep is hurt, heal itself
        if (creep.hits < creep.hitsMax) {
            creep.heal(creep);
        } else {
            //NEED TO ONLY DO THIS WHEN A FLAG IS SET?
            let closeCreep = creep.pos.findClosestByRange(FIND_MY_CREEPS, {filter: (closeCreep) => { return closeCreep.hits < closeCreep.hitsMax}});
            if (creep.heal(closeCreep) == ERR_NOT_IN_RANGE) {
                creep.moveTo(closeCreep);
            }
        }
        
    }
};


module.exports = roleVanguard;

/**
 * This module will assess the conditions of each room and assign it a room level
 * 
 * 1 = Room has no level assigned. Fresh room with nothing except a spawn and maybe few extensions
 * 2 = Room is level 1 and has at least One container within 2 range of every source in the room, also has Storage and at least 10 extensions.
 * 3 = Room is level 2 and has (roomm source count + 1) links.
 **/
module.exports = {
    checkRoomLevel: function(room) {
        
        if (!room.memory.level && room.find(FIND_MY_SPAWNS)[0] ) {
            room.memory.level = 1;
        }
        
        
        if (room.memory.level == 1) {
            
            //If there is no storage, return
            let storage = room.find(FIND_MY_STRUCTURES, {filter: {structureType : STRUCTURE_STORAGE}});
            if (storage.length < 1) {
                return; 
            }
            
            //If there are less than 10 extensions, return
            let extensions = room.find(FIND_MY_STRUCTURES, {filter: {structureType : STRUCTURE_EXTENSION}});
            if (extensions.length < 10) {
                return;
            }
            
            
            let sources = room.find(FIND_SOURCES);
            let containers = room.find(FIND_STRUCTURES, {filter: {structureType : STRUCTURE_CONTAINER}});
            
            //Find all sources with no container within 2 range
            let containerlessSources = sources.filter( source => {
                return source.pos.findInRange(containers, 2) == false;
            });
            
            //If there are any sources without a nearby container, return
            if (containerlessSources != 0 ) {
                return;
            }
            
            room.memory.level = 2;
        }
        
        if (room.memory.level == 2) {
            
        }
        
    }
};
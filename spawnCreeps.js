var constants = require('constants');

var spawnCreeps = {
    
    run: function(room) {
        //Get spawn and controller objects.
        var spawn = room.spawns[0];
        var controller = room.controller;
        
        
        if (!spawn) {
            //If there is no spawn in this room, exit the function
            return;
        }
        
        //Add in functionality for multiple spawns
        if (room.spawns.length > 1) {
            if (room.spawns[0].spawning) {
                spawn = room.spawns[1];
            }
        }
        
        
        //Initalize all worker counts to 0
        var numWorkHorses = 0;
        var numSpawnSlaves = 0;
        var numHarvesters = 0;
        var numUpgraders = 0;
        var numTransporters = 0;
        var numBuilders = 0;
        
        //Add in function to find room tier?
        
        var spawnStratChangePoint = 4;
        //Set desired creep amounts for each room level
        if (controller.level < spawnStratChangePoint) {
            numWorkHorses = 21;
        }
        
        if (controller.level >= spawnStratChangePoint ) {
            numSpawnSlaves = 1;
            numHarvesters = 2;
            numUpgraders = (room.storage.store[RESOURCE_ENERGY] > 150000 ? 4 : 1);
            numUpgraders = (room.storage.store[RESOURCE_ENERGY] > 400000 ? 6 : numUpgraders);
            numTransporters = 2;
            //Only spawn a builder if there is a construction site
            if (room.find(FIND_CONSTRUCTION_SITES).length > 0) {
                numBuilders = 1;
            } else {
                numBuilders = 0;
            }
        }
        
        //check number of creeps in room and spawn based off that
        if (controller.level < spawnStratChangePoint) {
            var workHorses = _.filter(Game.creeps, { memory: {role : 'workHorse', home: room.name}});
            if (_.size(workHorses) < numWorkHorses) {
                spawn.createCreep(constants.creeps['workHorse'], undefined, {role: 'workHorse', home: room.name, home_reached: 1});
            }
        }
        
        if (controller.level >= spawnStratChangePoint) {
            
            var spawnSlaves  = _.filter(Game.creeps, { memory: {role: 'spawnSlave', home: room.name}});
            var harvesters   = _.filter(Game.creeps, { memory: {role : 'harvester', home: room.name, destination: room.name}});
            var transporters = _.filter(Game.creeps, { memory: {role: 'transporter', home: room.name, destination: room.name}});
            var upgraders    = _.filter(Game.creeps, { memory: {role : 'upgrader', home: room.name}});
            var builders     = _.filter(Game.creeps, { memory: {role : 'builder', home: room.name}});
            
            //console.log(room.name + " : " +JSON.stringify(room.getCreepCount()));
            
            try{
                numSpawnSlaves = (spawnSlaves[0].ticksToLive < 150 ? numSpawnSlaves + 1 : numSpawnSlaves); 
                numSpawnSlaves = (spawnSlaves[0].body.length < 12  ? numSpawnSlaves + 1 : numSpawnSlaves);
            }catch(err){
                console.log("No spawn slaves in " + room.name);
            }
            if (_.size(spawnSlaves) < numSpawnSlaves) {
                let spawnSlaveBody = [];
                let iterations = Math.floor(room.energyAvailable / 150);
                
                
                for(let i = 0; i < iterations; i++) {
                    spawnSlaveBody.push(MOVE);
                    spawnSlaveBody.push(CARRY);
                    spawnSlaveBody.push(CARRY);
                    
                    if (i >= 15) {
                        break;
                    }
                }
                
                spawn.createCreep(spawnSlaveBody, undefined, {role: 'spawnSlave', home: room.name});
                return;
            }
            
            let harvesterCount = _.size(harvesters);
            if (harvesterCount < numHarvesters) {
                let sideNeeded = 'left';
                if (harvesterCount != 0 ){
                    sideNeeded = (harvesters[0].memory.source == constants[room.name].right ? 'left' : 'right')
                }
                spawn.createCreep(constants.creeps['harvester'], undefined, {role: 'harvester', source: constants[room.name][sideNeeded], home: room.name, destination: room.name});
                return;
            }
            
            let transporterCount = _.size(transporters);
            if (transporterCount < numTransporters) {
                let sideNeeded = 'left';
                if (transporterCount != 0) {
                    sideNeeded = (transporters[0].memory.source == constants[room.name].right ? 'left' : 'right');
                }
                spawn.createCreep(constants.creeps['transporter'], undefined, {role: 'transporter', source: constants[room.name][sideNeeded], home: room.name, destination: room.name});
                return;
            }
    
            if (_.size(upgraders) < numUpgraders) {
                spawn.createCreep(constants.creeps['upgrader'], undefined, {role: 'upgrader', home: room.name});
            }
            if (_.size(builders) < numBuilders) {
                spawn.createCreep(constants.creeps['builder'], undefined, {role: 'builder', home: room.name});
            }
        }
        
        this.spawnLongRangeMiners(room, spawn);
        this.spawnLongRangeMiningOperation(room, spawn);
        
    },
    
    spawnLongRangeMiners: function(room, spawn) {
        if(!constants.longRangeMiningv1.hasOwnProperty(room.name)){
            return;
        }
        
        constants.longRangeMiningv1[room.name].forEach( destinationRoomName => {
            
            
            let squatters       = _.filter(Game.creeps, { memory: {role : 'squatter', destination: destinationRoomName}});
            let longRangeMiners = _.filter(Game.creeps, { memory: {role : 'longRangeMiner', destination : destinationRoomName}});
            let reservers       = _.filter(Game.creeps, { memory: {role : 'reserver', destination: destinationRoomName}});
            let bodyguards      = _.filter(Game.creeps, { memory: {role : 'bodyguard', destination: destinationRoomName}});
            
            if (_.size(squatters) < 1) {
                spawn.createCreep(constants.creeps['squatter'], undefined, {role: 'squatter', destination: destinationRoomName});
                return;
            }
            if (_.size(reservers) < 1 ) {
                spawn.createCreep(constants.creeps['reserver'], undefined, {role: 'reserver', destination: destinationRoomName});
                return;
            }
            
            if (_.size(bodyguards) < 1) {
                spawn.createCreep(constants.creeps['bodyguard'], undefined, {role: 'bodyguard', destination: destinationRoomName, home: room.name});
                return;
            }
            
            if (_.size(longRangeMiners) < 4) {
                spawn.createCreep(constants.creeps['longRangeMiner'], undefined, {role: 'longRangeMiner', destination: destinationRoomName});
            }  
        });
    },
    
    spawnLongRangeMiningOperation: function(room, spawn) {
        if(!constants.longRangeMining.hasOwnProperty(room.name)){
            return;
        }
        
        constants.longRangeMining[room.name].forEach( destinationRoomName => {
            
            //let squatters        = _.filter(Game.creeps, { memory: {role : 'squatter', destination: destinationRoomName}});
            let longRangeWorkers = _.filter(Game.creeps, { memory: {role : 'longRangeWorker', destination : destinationRoomName}});
            let harvesters       = _.filter(Game.creeps, { memory: {role : 'harvester', destination: destinationRoomName}});
            let transporters     = _.filter(Game.creeps, { memory: {role : 'transporter', destination: destinationRoomName}});
            let bodyguards       = _.filter(Game.creeps, { memory: {role : 'bodyguard', destination: destinationRoomName}});
            let reservers        = _.filter(Game.creeps, { memory: {role : 'reserver', destination: destinationRoomName}});

            
            let wantedLongRangeWorkers  = 2;
            let wantedHarvesters        = 1;//Game.rooms[destinationRoomName].sources.length;
            let wantedTransporters      = 2;//Game.rooms[destinationRoomName].sources.length *3;
            let wantedBodyguards        = 1;
            
            try{
                wantedHarvesters        = Game.rooms[destinationRoomName].sources.length;
                wantedTransporters      = Game.rooms[destinationRoomName].sources.length *2; 
            }catch(err){
                console.log('no creeps in destination room ' + destinationRoomName +', spawnCreeps.js');
            }
            
            let farRooms = ['E52S77'];
            
            if (farRooms.indexOf(destinationRoomName) != -1){
                wantedTransporters = wantedTransporters * 2;
            }
            
            //if (_.size(squatters) < 1) {
            //    spawn.createCreep(constants.creeps['squatter'], undefined, {role: 'squatter', destination: destinationRoomName, home: room.name});
            //    return;
            //}
            
            if (_.size(reservers) < 1 ) {
                spawn.createCreep(constants.creeps['reserver'], undefined, {role: 'reserver', destination: destinationRoomName});
                return;
            }
            
            if (_.size(harvesters) < wantedHarvesters) {
                spawn.createCreep(constants.creeps['harvester'], undefined, {role: 'harvester', destination: destinationRoomName, home: room.name});
                return;
            }
            if (_.size(bodyguards) < wantedBodyguards) {
                spawn.createCreep(constants.creeps['bodyguard'], undefined, {role: 'bodyguard', destination: destinationRoomName, home: room.name});
                return;
            }
            if (_.size(transporters) < wantedTransporters) {
                spawn.createCreep(constants.creeps['transporter'], undefined, {role: 'transporter', destination: destinationRoomName, home: room.name});
                return;
            }
            if (_.size(longRangeWorkers) < wantedLongRangeWorkers) {
                spawn.createCreep(constants.creeps['longRangeWorker'], undefined, {role: 'longRangeWorker', destination: destinationRoomName, home: room.name});
            }
        });
    }
}

module.exports = spawnCreeps;
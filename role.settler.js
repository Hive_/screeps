
var roleSettler = {
    run: function(creep) {
        
        if (creep.pos.getRangeTo(Game.flags['settle'].pos) == 0) {
            creep.memory.settle_reached = 1;
        }
        
        if (creep.memory.settle_reached != 1) {
            creep.moveTo(Game.flags['settle']);
            return;
        }
        
        if (creep.memory.settle_reached == 1) {
            creep.claimController(creep.room.controller);
        }
    }
};


module.exports = roleSettler;

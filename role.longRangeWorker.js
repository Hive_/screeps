var roleLongRangeWorker = {

    /** @param {Creep} creep **/
    run: function(creep) {  
        let destinationRoom = Game.rooms[creep.memory.destination];
        
        if (!creep.memory.currentAction) {
            creep.memory.currentAction = 'fill';
        }
        
        //if (creep.room.name != creep.memory.destination && creep.memory.currentAction != 'fill') {
        //    creep.moveTo(Game.flags[creep.memory.destination]);
        //    return;
        //}

        if (creep.carry.energy == 0) {
            creep.memory.currentAction = 'fill';
            
            
            //let energyStores = creep.room.find(FIND_STRUCTURES, {filter:  {structureType: STRUCTURE_CONTAINER}});
            //energyStores = energyStores.sort( (a,b) => {
            //    return a.store[RESOURCE_ENERGY] - b.store[RESOURCE_ENERGY];
            //});
            //creep.memory.container = energyStores[energyStores.length - 1].id;
        }
        if (creep.carry.energy == creep.carryCapacity) {
            
            if (destinationRoom.constructionSites.length > 0) {
                creep.memory.currentAction = 'build'
            } else if (destinationRoom.damagedStructures.length > 0) {
                creep.memory.currentAction = 'repair'
            }
        }
        
        if ((creep.memory.currentAction == 'repair' && destinationRoom.damagedStructures.length == 0) || (creep.memory.currentAction == 'build' && destinationRoom.constructionSites.length ==  0 )) {
            creep.memory.currentaAction = 'fill';
        }
        
        
        if (creep.memory.currentAction == 'build') {
            if(creep.build(destinationRoom.constructionSites[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(destinationRoom.constructionSites[0]);
            }
        }
        if (creep.memory.currentAction == 'repair') {
            if(creep.repair(destinationRoom.damagedStructures[destinationRoom.damagedStructures.length - 1]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(destinationRoom.damagedStructures[destinationRoom.damagedStructures.length - 1]);    
            }
        }
        if (creep.memory.currentAction == 'fill') {
            //let container = Game.getObjectById(creep.memory.container);
            if(creep.withdraw(Game.rooms[creep.memory.home].storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(Game.rooms[creep.memory.home].storage);
            }
        }
        
    }
};

module.exports = roleLongRangeWorker;
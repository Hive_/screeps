
var roleSpawnSlave = {
    run: function(creep) {
        if (creep.carry.energy == 0 ) {
            creep.memory.transporting = false;
        } else {
            creep.memory.transporting = true;
        }
        
        if (creep.ticksToLive < 100) {
            this.transferAndSuicide(creep);
            return;  
        }
        
        if(creep.memory.transporting == false) {
            
            if (creep.room.storage){
                if (creep.room.storage.transfer(creep, RESOURCE_ENERGY, creep.carryCapacity) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(creep.room.storage);
                }
            }
        }
        else {
            
            if(!creep.memory.spawnStructures || creep.memory.spawnStructures.length != creep.room.spawnStructures.length) {
                let sortedStructures = creep.room.spawnStructures.sort( (a,b) => {
                    return creep.pos.getRangeTo(a.pos) - creep.pos.getRangeTo(b.pos);
                });
                
                creep.memory.spawnStructures = sortedStructures.map( structure => {
                    return structure.id;
                });
            }
            
            if(creep.memory.spawnStructures.length > 0) {
                let targetStructure = Game.getObjectById(creep.memory.spawnStructures[0]);
                let transferResult = creep.transfer(targetStructure, RESOURCE_ENERGY);
                if(transferResult == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targetStructure);
                }
                if (transferResult == ERR_FULL) {
                    //creep.memory.spawnStructures = creep.memory.spawnStructures.shift();
                    creep.memory.spawnStructures.shift();
                }
            }
            
        }
    },
    
    transferAndSuicide: function(creep) {
        //Go deposit all source, then immediately kill yourself.
        if (creep.transfer(creep.room.storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
            creep.moveTo(creep.room.storage);
        }
        if (creep.carry.energy == 0){
            creep.suicide();
        }
    }
};


module.exports = roleSpawnSlave;

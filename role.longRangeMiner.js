/**
 * REQUIRED MEMORY:
 * 
 * role
 * destination : room name of the target room 
 **/
var roleLongRangeMiner = {

    /** @param {Creep} creep **/
    run: function(creep) {
try{
        /* Initialize memory values for creep if any were ommitted */
        if (!creep.memory.source) {
            creep.assignSource(creep.memory.destination);
        }
        let targetSource = Game.getObjectById(creep.memory.source);
        if (!creep.memory.path) {
            creep.setPathTo(targetSource);
            //creep.setPathTo(destinationSource);
        }
        if (!creep.memory.currentAction) {
            creep.memory.currentAction = 'mining';
        }
        if (!creep.memory.home) {
            creep.memory.home = creep.room.name;
        }
        
        
        /*Set creep currentAction value for down the line*/
        if (creep.memory.currentAction != 'mining' && creep.carry.energy == 0) {
            creep.memory.currentAction = 'mining';
            //creep.setPathTo(destinationSource);  Not necessary because we are not using moveByPath to get back to the drop off location, so it always has path = the source.
        }
        if (creep.memory.currentAction == 'mining' && creep.carry.energy == creep.carryCapacity) {
            creep.memory.currentAction = 'building';
        }
        
        if (creep.memory.currentAction == 'building' && creep.room.constructionSites.length == 0) {
            //creep.memory.currentAction = 'repairing';
            creep.memory.currentAction = 'delivering';
        }
        
        if (creep.memory.currentAction == 'repairing' && creep.room.damagedStructures.length == 0) {
            creep.memory.currentAction = 'delivering';
        }
        
        
        /*
        if (creep.memory.currentAction == 'working'){
            if (creep.room.constructionSites.length > 0) {
                console.log('setting creep action to building');
                creep.memory.currentAction = 'building';
                console.log(creep.memory.currentAction);
            } else if (creep.room.damagedStructures.length > 0) {
                creep.memory.currentAction = 'repairing';  
            } else {
                creep.memory.currentAction = 'delivering';
            }
        }
        
        //FIX THIS HIDEOUS SHIT... Creeps get stuck in building or repairing, this gets them out of that rut... for now!
        if (creep.memory.currentAction != 'mining' && creep.memory.currentAction != 'delivering' && creep.room.damagedStructures.length == 0 && creep.room.constructionSites.length == 0) {
            creep.memory.currentAction = 'working'
        }
        */
        
        /*Do work based on the creep's currentAction value*/
        if (creep.memory.currentAction == 'mining') {
            if (creep.harvest(targetSource) == ERR_NOT_IN_RANGE){
                creep.moveTo(targetSource, {reusePath: 10});
            }
            /*Commending out broken fucking move by path bullshit
            if (creep.moveByPath(creep.memory.path) == ERR_NOT_FOUND) {
                creep.setPathTo(targetSource);
            }*/
            //No need to do move closer.... i think
        }
        if (creep.memory.currentAction == 'building') {
            creep.smartBuild();
        }
        
        //if (creep.memory.currentAction == 'repairing') {
        //    creep.smartRepair();
        //}
        
        if (creep.memory.currentAction == 'delivering') {
            let nearbyDamagedStructures = creep.pos.findInRange(creep.room.damagedStructures, 1);
            if(nearbyDamagedStructures.length > 0){
                creep.repair(nearbyDamagedStructures[0]);
                return;
            }
            creep.smartDeliver();
        }
} catch(err){
    console.log(err);
}
        
    }
};

module.exports = roleLongRangeMiner;
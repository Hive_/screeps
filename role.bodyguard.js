

var roleBodyguard = {
    run: function(creep) {
        
        if (creep.memory.renewing == null){
            creep.memory.renewing = false;
        }
        
        let roomIsSafe = true;
        if (creep.room.hostileCreeps.length > 0) {
            roomIsSafe = false;
        }
        
        //If the creep is going to die, go renew.
        if (creep.ticksToLive < 100 && roomIsSafe ) {
            creep.memory.renewing = true;
        }
        
        if (creep.memory.renewing == true && creep.ticksToLive > 1450){
            creep.memory.renewing = false;
        }
        
        if (creep.memory.renewing == true) {
            creep.renew();
            return;
        }
        
        if (!creep.memory.renewing && roomIsSafe) {
            creep.moveTo(Game.flags[creep.memory.destination]);
        }
        
        //If there are hostile creeps, kill them
        if (!roomIsSafe) {
            let hostileTarget = creep.room.hostileCreeps[0];
            creep.moveTo(hostileTarget);
            creep.attack(hostileTarget);
            creep.heal(creep);
        }
            
        //If creep is hurt, heal itself
        if (creep.hits < creep.hitsMax) {
            creep.heal(creep);
        }
        
    }
    
};


module.exports = roleBodyguard;

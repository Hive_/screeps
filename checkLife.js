//If a creep is too old, kill it before it wastes energy.
var checkLife = {
    run: function(creep) {
        if (creep.ticksToLive < 50 && creep.carry.energy == 0 && creep.memory.role != 'bodyguard') {
            creep.suicide();
        }
    }
}

module.exports = checkLife;

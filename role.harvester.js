var roleHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {
        //Assign source if creep doesnt have one
        if (!creep.memory.source) {
            if (creep.memory.destination != null) {
                creep.assignSource(creep.memory.destination);
            } else {
                creep.assignSource();
            }
        }
        
        if(!creep.memory.currentAction) {
            creep.memory.currentAction = 'mining';
        }
        
        if (creep.carry.energy == 0) {
            creep.memory.currentAction = 'mining';
        }
        if (creep.carry.energy == creep.carryCapacity) {
            creep.memory.currentAction = 'deposit';
        }
        
        if (creep.memory.currentAction == 'mining') {
            let source = Game.getObjectById(creep.memory.source);
            if(creep.harvest(source) == ERR_NOT_IN_RANGE) {
                creep.moveTo(source);
            }
        }
        if (creep.memory.currentAction == 'deposit'){
            let sourceContainer = creep.getSourceContainer()
            if (creep.repairClose() == 0){
                if (creep.transfer(sourceContainer, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(sourceContainer);
                }
            }
            //creep.repairClose();
            //creep.buildClose();
            
            
        }
    }
};

module.exports = roleHarvester;
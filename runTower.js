
var runTower = {
    
    run: function(room) {
        
        var towers = room.find(FIND_MY_STRUCTURES,
            {filter: {structureType: STRUCTURE_TOWER}}
        );
        if (towers.length < 1) {
            //If there are no tower in this room, exit the function...
            return; 
        }
        
        towers.forEach( tower => {
            var closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
	        if (closestHostile) {
	            tower.attack(closestHostile);
	        } else if (room.damagedStructures.length > 0  && tower.energy > 800) {
	            tower.repair(room.damagedStructures[0]);
	        }
        });
        
        /**
         * Need to add in that if tower energy dips below a certain point, then signal that we need a dedicated creep
         **/
        
    }
}

module.exports = runTower;
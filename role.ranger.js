
var roleRanger = {
    run: function(creep) {
        if (creep.pos.getRangeTo(Game.flags['warpath'].pos) == 0) {
            creep.memory.warpath_reached = 1;
        }
        if (creep.pos.getRangeTo(Game.flags['wartarget'].pos) == 0) {
            creep.memory.wartarget_reached = 1;
        }
        
        //if (creep.memory.warpath_reached != 1) {
        //    creep.moveTo(Game.flags['warpath']);
        //    return;
        //}
        
        if (creep.memory.wartarget_reached != 1) {
            creep.moveTo(Game.flags['wartarget']);
            return;
        }
        
        if (creep.memory.blocked == 1) {
            let wall = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: { structureType: STRUCTURE_WALL }
            });
            
            if (creep.rangedAttack(wall) == ERR_NOT_IN_RANGE) {
                creep.moveTo(wall);
            }
            return;
        }
        
        var towers = creep.room.find(FIND_HOSTILE_STRUCTURES, {
            filter: { structureType: STRUCTURE_TOWER }
        });
        //If the attack happened, then we stop the rest of the code from executing because we want to do it again! ';..;'
        creep.moveTo(towers[0]);
        if (creep.rangedAttack(towers[0]) == OK) {
            return;
        }
        
        //Add code to make sure to stick to one target until it dies?
        var enemy = creep.pos.findClosestByPath(FIND_HOSTILE_CREEPS);
        if (!enemy) {
            let spawn = creep.room.find(FIND_HOSTILE_SPAWNS)[0];
            if(!spawn){
                let structures = creep.room.find(FIND_HOSTILE_STRUCTURES, {
                    filter : structure =>  structure.structureType != STRUCTURE_CONTROLLER
                });
                if (creep.rangedAttack(structures[0]) == ERR_NOT_IN_RANGE ) {
                    creep.moveTo(structures[0]);
                }
            } else {
                if (creep.rangedAttack(spawn) == ERR_NOT_IN_RANGE ) {
                    creep.moveTo(spawn);
                }
            }
        } else {
            if (creep.rangedAttack(enemy) == ERR_NOT_IN_RANGE) {
                creep.moveTo(enemy);
            }
            creep.moveTo(enemy);
        }
    }
};


module.exports = roleRanger;

/**
 * Move to flag, reserve that room's controler
 **/

module.exports = {

    run: function(creep) {
     
        if (creep.room.name == creep.memory.destination) {
            creep.memory.destination_reached = 1;
        }
     
        if (creep.memory.destination_reached == 1) {
            if( creep.reserveController(Game.rooms[creep.memory.destination].controller) == ERR_NOT_IN_RANGE) {
                let moveResult = creep.moveTo(Game.rooms[creep.memory.destination].controller);
            }
        } else {
            try{
                //creep.moveTo(Game.rooms[creep.memory.destination].controller);
                creep.moveTo(Game.flags[creep.memory.destination]);
            }catch(err){
                console.log("role.reserver "+err+" for creep " +creep.name+ " in "+creep.room.name);
            }
        }
        
    }

};
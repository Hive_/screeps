
//Game.spawns['The Hive'].createCreep([WORK,CARRY,MOVE,MOVE], undefined, {role: 'workHorse', source: 'SOURCE HERE', home: 'E51S76', home_reached: 0});
var roleWorkHorse = {
    run: function(creep) {
        
        
        if (creep.room.name == creep.memory.home) {
            creep.memory.home_reached = 1;
        }

        if (creep.memory.home_reached != 1) {
            creep.moveTo(Game.rooms[creep.memory.home].controller);
            //creep.moveTo(Game.flags['waypoint']);
            return;
        }
        
        //Assign source if creep doesnt have one
        if(!creep.memory.source && creep.memory.home_reached == 1){
            creep.moveTo(creep.room.controller);
            creep.assignSource();
        }
        
        //Mine minerals
        if (creep.carry.energy == 0) {
            creep.memory.action = 'mining';
        } else if (creep.carry.energy == creep.carryCapacity) {
            creep.memory.action = 'working';
        }
        if (creep.memory.action == 'mining') {
            /*Code for picking up dropped energy*/
            //if (creep.room.droppedEnergy.length > 0){
            //    if(creep.pickup(creep.room.droppedEnergy[0]) == ERR_NOT_IN_RANGE) {
            //        creep.moveTo(creep.room.droppedEnergy[0]);
            //        return;
            //    }
            //}
            /*end energy pickup code*/
            
            let source = Game.getObjectById(creep.memory.source);
            if(creep.harvest(source) == ERR_NOT_IN_RANGE) {
                creep.moveTo(source);
                return;
            }
        }
        
        //feed spawn
        if (creep.memory.action == 'working') {
            if(creep.room.spawnStructures.length > 0) {
                if(creep.transfer(creep.room.spawnStructures[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(creep.room.spawnStructures[0]);
                }
            } else {
                if (creep.room.constructionSites.length > 0){
                    creep.memory.action = 'building';
                } else {
                    creep.memory.action = 'upgrading';
                }
            }
        }
        
        if (creep.memory.action == 'building') {
            if (creep.room.constructionSites.length == 0){
                creep.memory.action = 'mining';
            } else {
                creep.moveTo(creep.room.constructionSites[0]);
                creep.build(creep.room.constructionSites[0]);
            }
        }
        
        //if spawn is full upgrade controller
        if (creep.memory.action == 'upgrading') {
            creep.moveTo(creep.room.controller);
            creep.upgradeController(creep.room.controller);
        }
        
    }
};


module.exports = roleWorkHorse;

var roleHarvester = require('role.harvester');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');
var roleTransporter = require('role.transporter');
var roleWarrior = require('role.warrior');
var roleRanger = require('role.ranger');
var roleScout = require('role.scout');
var roleSpawnSlave = require('role.spawnSlave');
var roleVanguard = require('role.vanguard');
var roleBodyguard = require('role.bodyguard');
var roleLongRangeMiner = require('role.longRangeMiner');
var roleLongRangeWorker = require('role.longRangeWorker');
var roleSettler = require('role.settler');
var roleWorkHorse = require('role.workHorse');
var roleReserver = require('role.reserver');
var roleSquatter = require('role.squatter');
var roleLongRangeWorker = require('role.longRangeWorker');

var runTask = {
    run: function(creep) {
        
        if (creep.memory.role == 'harvester') {
            roleHarvester.run(creep);
        }
        if (creep.memory.role == 'upgrader') {
            roleUpgrader.run(creep);
        }
        if (creep.memory.role == 'builder') {
            roleBuilder.run(creep);
        }
        if (creep.memory.role == 'transporter') {
            roleTransporter.run(creep);
        }
        if (creep.memory.role == 'scout') {
            roleScout.run(creep);
        }
        if (creep.memory.role == 'warrior') {
            roleWarrior.run(creep);
        }
        if (creep.memory.role == 'ranger') {
            roleRanger.run(creep);
        }
        if (creep.memory.role == 'spawnSlave') {
            roleSpawnSlave.run(creep);
        }
        if (creep.memory.role == 'vanguard') {
            roleVanguard.run(creep);
        }
        if (creep.memory.role == 'bodyguard') {
            roleBodyguard.run(creep);
        }
        if (creep.memory.role == 'longRangeMiner') {
            roleLongRangeMiner.run(creep);
        }
        if (creep.memory.role == 'settler') {
            roleSettler.run(creep);
        }
        if (creep.memory.role == 'workHorse') {
            roleWorkHorse.run(creep);
        }
        if (creep.memory.role == 'reserver') {
            roleReserver.run(creep);
        }
        if (creep.memory.role == 'squatter') {
            roleSquatter.run(creep);
        }
        if (creep.memory.role == 'longRangeWorker') {
            roleLongRangeWorker.run(creep);
        }
    }
}

module.exports = runTask;
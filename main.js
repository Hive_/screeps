var runTask = require('runTask');
var runTower = require('runTower');
var checkLife = require('checkLife');
var spawnCreeps = require('spawnCreeps');
var linkActions = require('linkActions');
var roomLeveler = require('roomLeveler');
var defenseEmergency = require('defenseEmergency'); 
require('extensions');

module.exports.loop = function () {
    
    //Delete memory of dead creeps... T_T
    for(var i in Memory.creeps) {
        if(!Game.creeps[i]) {
            delete Memory.creeps[i];
        }
    }
    
    //first loop through rooms and set the room object up
    //This double loop is sloppy and needs to change.  But it's quick and it works for now...
    for (let roomName in Game.rooms) {
        Game.rooms[roomName].setRoomObjects();
    }
    
    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        runTask.run(creep);
        checkLife.run(creep);
    }
    
    //Then loop through rooms and do actions
    for (var roomName in Game.rooms) {
        //if (Game.rooms[roomName].myRoom()) {  
            var room = Game.rooms[roomName];
            //room.setRoomObjects();
            spawnCreeps.run(room);
            runTower.run(room);
            defenseEmergency.run(room);
            //linkActions.funnel(roomName);   Need to figure out room levels first.
            //roomLeveler.checkRoomLevel(room);
        //}
    }
    
    
    



    
    /**
     * Put Jankey ass code here
     **/
    //spawnScout();
    //spawnLongRangeMiners();
    //spawnWorkHorses();
    //spawnReservers();
    //spawnSwarm();
    //spawnRaiders();
    
}

var constants = require('constants');
function spawnLongRangeMiners(){
    var numLongRangeMiners = _.filter(Game.creeps, { memory: {role : 'longRangeMiner'}});
    if (_.size(numLongRangeMiners) < 4) {
        Game.spawns['Spawn1'].createCreep(constants.creeps['longRangeMiner'], undefined, {role: 'longRangeMiner', destination: 'E51S77'});
    }
}

function spawnScout(){
    var scouts = _.filter(Game.creeps, { memory: {role : 'scout'}});
    if (_.size(scouts) < 1) {
        Game.spawns['Spawn1'].createCreep(constants.creeps['scout'], undefined, {role: 'scout'});
    }
}

function spawnWorkHorses(){
    var numWorkHorses = _.filter(Game.creeps, { memory: {role : 'workHorse'}});
    if (_.size(numWorkHorses) < 20) {
        Game.spawns['The Hive'].createCreep([WORK, CARRY, MOVE, MOVE], undefined, {role: 'workHorse', home: 'E55S77', home_reached: 0});
        Game.spawns['Spawn1'].createCreep([WORK, CARRY, MOVE, MOVE], undefined, {role: 'workHorse', home: 'E55S77', home_reached: 0});
    }
}

function spawnReservers(){
    var reservers = _.filter(Game.creeps, {memory: {role : 'reserver'}});
    if (_.size(reservers) < 1 ) {
        Game.spawns['The Hive'].createCreep([CLAIM, CLAIM, MOVE, MOVE, MOVE], undefined, {role: 'reserver'});
    }
}

function spawnRaiders(){
    Game.spawns['The Hive'].createCreep(constants.creeps['raider'], undefined, {role: 'warrior', warpath_reached: 0, wartarget_reached: 0, blocked: 0});
    Game.spawns['Spawn2'].createCreep(constants.creeps['raider'], undefined, {role: 'warrior', warpath_reached: 0, wartarget_reached: 0, blocked: 0});
}

function spawnSwarm(){
    var numSwarm = _.filter(Game.creeps, { memory: {role : 'warrior'}});
    if (_.size(numSwarm) < 20) {
        Game.spawns['The Hive'].createCreep(constants.creeps['ranger'], undefined, {role: 'warrior', warpath_reached: 0, wartarget_reached: 0, blocked: 0});
        //Game.spawns['Spawn1'].createCreep(constants.creeps['warrior'], undefined, {role: 'warrior', warpath_reached: 0, wartarget_reached: 0, blocked: 0});
    }
}
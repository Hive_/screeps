//TODO: Remove travel by path...
var roleTransporter = {
    run: function(creep) {
        //Assign source if creep doesnt have one
        if (!creep.memory.source) {
            if (creep.memory.destination != null) {
                creep.assignSource(creep.memory.destination);
            } else {
                creep.assignSource();
            }
        }
        
        if (!creep.memory.conatiner) {
            creep.memory.container = creep.getSourceContainer().id;
        } 
        
        let path = null;
        if (creep.carry.energy == 0 && creep.memory.currentAction == 'deliver' || !creep.memory.currentAction) {
            creep.memory.currentAction = 'get';
            //path = creep.pos.findPathTo(creep.getSourceContainer());
            //creep.memory.path = Room.serializePath(path);
            
        } else if(creep.carry.energy == creep.carryCapacity && creep.memory.currentAction == 'get') {
            creep.memory.currentAction = 'deliver';
            //path = creep.pos.findPathTo(creep.room.storage);
            //path = creep.pos.findPathTo(Game.rooms[creep.memory.home].storage);
            //creep.memory.path = Room.serializePath(path);
        }
        
        if(creep.memory.currentAction == 'get') {
            try{
                //let container = creep.getSourceContainer();
                let container = Game.getObjectById(creep.memory.container);
                let result = container.transfer(creep, RESOURCE_ENERGY);
                if (result == ERR_NOT_IN_RANGE) {
                    creep.moveTo(container);
                }
                if (result == ERR_NOT_ENOUGH_RESOURCES) {
                    //Move to the container if it is more than 1 square away.
                    if (!creep.pos.inRangeTo(container, 2)) {
                        creep.moveTo(container);
                    }
                }
            }catch(err){
                console.log(err + " in transporter");
            }
            //if (result == ERR_NOT_IN_RANGE) {
            //    if (creep.moveByPath(creep.memory.path) == ERR_NOT_FOUND) {
            //        creep.memory.currentAction = null;
            //    }
            //} 

        }
        else if(creep.memory.currentAction == 'deliver') {
            if (creep.room.storage){
                if (creep.transfer(creep.room.storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    //creep.moveByPath(creep.memory.path);
                    creep.moveTo(creep.room.storage);
                }
            } else {
                creep.moveTo(Game.rooms[creep.memory.home].storage);
            }
        }
        
        
    }
};


module.exports = roleTransporter;

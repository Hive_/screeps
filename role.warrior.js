

var roleWarrior = {
    run: function(creep) {
        
        //If we are being blocked, then we need to kill a wall.
        
        
        if (creep.pos.getRangeTo(Game.flags['warpath'].pos) == 0) {
            creep.memory.warpath_reached = 1;
        }
        if (creep.pos.getRangeTo(Game.flags['wartarget'].pos) == 0) {
            creep.memory.wartarget_reached = 1;
        }
        
        //if (creep.memory.warpath_reached != 1) {
        //    creep.moveTo(Game.flags['warpath']);
        //    return;
        //}
        
        if (creep.memory.wartarget_reached != 1) {
            creep.moveTo(Game.flags['wartarget']);
            return;
        }
        
        if (creep.memory.blocked == 1) {
            let wall = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: { structureType: STRUCTURE_WALL }
            });
            
            if (creep.attack(wall) == ERR_NOT_IN_RANGE) {
                creep.moveTo(wall);
            }
            return;
        }
        
        var towers = creep.room.find(FIND_HOSTILE_STRUCTURES, {
            filter: { structureType: STRUCTURE_TOWER }
        });
        
        if (creep.attack(towers[0]) == ERR_NOT_IN_RANGE) {
            creep.moveTo(towers[0]);
            return;
        }
        
        var enemy = creep.pos.findClosestByPath(FIND_HOSTILE_CREEPS);
        if (!enemy) {
            let spawn = creep.room.find(FIND_HOSTILE_SPAWNS)[0];
            if (creep.attack(spawn) == ERR_NOT_IN_RANGE ) {
                creep.moveTo(spawn);
            }
        }
        
        if (creep.attack(enemy) == ERR_NOT_IN_RANGE) {
            creep.moveTo(enemy)
        }
        
    }
};


module.exports = roleWarrior;

var constants = require('constants');

module.exports = {
    funnel: function(roomName) {
        
        if (!constants[roomName] || !constants[roomName].links){
            return;
        }
        
        var givers = [];
        
        constants[roomName].links.givers.forEach(linkId => {
            givers.push(Game.getObjectById(linkId));
        });
        console.log(givers);
    }
};
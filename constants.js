
module.exports = {
    
    home : {
        id    : 'E51S78',
        left  : '5836b8538b8b9619519f1d56',
        right : '5836b8538b8b9619519f1d55',
        store : '584124f32c80f7bb79776fce'
    },
    
    right : {
        id : 'E52S78',
        source: '5836b8568b8b9619519f1d99'
    },
    
    downRight : {
        id : 'E52S79'
    },
    
    'E51S78' : {
        min_wall : 10000,
        left     : '5836b8538b8b9619519f1d56',
        right    : '5836b8538b8b9619519f1d55',
        store    : '584124f32c80f7bb79776fce',
        links    : {
            givers : ['584c586319f788821f8fba60', '584c4d691dfc07f770ad3a5d'],
            takers : ['584c5c778f0153db20d076ba']
        }
    },
    
    'E51S76' : {
        left     : '5836b8528b8b9619519f1d4f',
        right    : '5836b8528b8b9619519f1d4d',
        store    : '584a57511c432bae180aface',
        
    },
    
    'E55S77' : {
        left     : '5836b85e8b8b9619519f1e95',
        right    : '5836b85e8b8b9619519f1e96',
        //store    : '5859580f39d12e5334681841',
        
    },
    
    creeps : {
        'spawnSlave'     : [CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE],
        'harvester'      : [MOVE,MOVE,MOVE,WORK,WORK,WORK,WORK,WORK,WORK,CARRY],
        'transporter'    : [MOVE,MOVE,MOVE,MOVE,MOVE,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY],
        'upgrader'       : [MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,WORK,WORK,WORK,WORK,WORK,WORK,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY],
        'builder'        : [WORK, WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, MOVE, MOVE, MOVE],
        'workHorse'      : [WORK, CARRY, MOVE, MOVE],
        'warrior'        : [TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,ATTACK,ATTACK,ATTACK,ATTACK],
        'ranger'         : [TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,RANGED_ATTACK],
        'raider'         : [MOVE,MOVE,MOVE,MOVE,MOVE,ATTACK,ATTACK,ATTACK],
        'bodyguard'      : [MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,HEAL,HEAL,HEAL,ATTACK,ATTACK,ATTACK],
        'longRangeWorker': [MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,WORK,WORK,WORK,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY],
        'longRangeMiner' : [MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,WORK,WORK,WORK,WORK,WORK,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY],
        'reserver'       : [CLAIM, CLAIM, MOVE, MOVE],
        'scout'          : [MOVE, MOVE],
        'squatter'       : [MOVE],
        'vanguard'       : [TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,TOUGH,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,MOVE,HEAL,HEAL,HEAL,HEAL,HEAL,HEAL,HEAL,HEAL,HEAL,HEAL,HEAL,HEAL,HEAL,HEAL,HEAL],
        
    },
    
    longRangeMining : {
        'E51S76' : ['E51S77', 'E51S75'],
        'E51S78' : ['E52S78', 'E52S77'],
        'E55S77' : ['E55S78', 'E54S77'],
    },
    
    longRangeMiningv1 : {
    
    },
    
    maintain : {
        'E51S78' : ['E52S78'],
    }
};
var roleUpgrader = {

    /** @param {Creep} creep **/
    run: function(creep) {

        if(creep.carry.energy == 0) {
            creep.memory.upgrading = false;
        }
        if(creep.carry.energy == creep.carryCapacity) {
            creep.memory.upgrading = true;
        }
        
        if(creep.memory.upgrading) {
            creep.moveTo(creep.room.controller);
            creep.upgradeController(creep.room.controller);
        }
        else {
            if(creep.withdraw(creep.room.storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.storage);
            }
        }
    }
};

module.exports = roleUpgrader;